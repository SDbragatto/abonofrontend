import React from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

//import Logon from './Logon';
import Profile from './Profile';
import Arquivado from './OperacaoView';
import NewOperacao from './NewOperacao';
import Missoes from './Missoes';
import PlanilhaOperacao from './PlanilhaOperacao';
import PlanilhaGeral from './PlanilhaGeral';
import PlanilhaEfetivo from './PlanilhaEfetivo';

export default function Routes(){
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Profile} />
                <Route path="/profile" component={Profile} />
                <Route path="/operacao/view" component={Arquivado} />
                <Route path="/novaoperacao" component={NewOperacao} />
                <Route path="/editOperacao/:id" component={NewOperacao} />
                <Route path="/missoes/view/:id" component={Missoes} />
                <Route path="/planilha/operacao/:id" component={PlanilhaOperacao} />
                <Route path="/planilhaGeral" component={PlanilhaGeral} />
                <Route path="/efetivoGeral" component={PlanilhaEfetivo} />
                
            </Switch>
        </BrowserRouter>
    );
}
//--openssl-legacy-provider