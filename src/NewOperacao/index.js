import React, { useEffect, useState } from 'react';
import {Link, useParams, useHistory} from 'react-router-dom';
import {FiArrowLeft} from 'react-icons/fi';

import api from "../service/api";

import './style.css';

export default function NewOperacao(){
    
    const { id } = useParams(); // pega a parms na url
    const [titulo, setTitulo] = useState('');
    const [cotaAprovada, setCota] = useState('');
    const [informacao, setInformacao] = useState('');
    //const [ini, setIni] = useState(''); // convertido pelo getTime
    //const [ter, setTer] = useState(''); // convertido pelo getTime
    // informações para impressão
    const [coint, setCoint] = useState('');
    const [opm, setOpm] = useState('');
    const [linhaTitulo, setLinhaTitulo] = useState('PLANILHA DE GRATIFICAÇÃO DE COMPLEMENTAÇÃO DE JORNADA OPERACIONAL');
    const [linhaMes, setLinhaMes] = useState('');
    const [linhaRef1, setLinhaRef1] = useState('');
    const [linhaRef2, setLinhaRef2] = useState('');
    const [dataIni, setDataIni] = useState('');
    const [dataTer, setDataTer] = useState('');
    const [linhaAssinData, setLinhaAssinData] = useState('');
    const [linhaAssinNome, setLinhaAssinNome] = useState('');
    const [status , setStatus] = useState();

    const history = useHistory();

    useEffect( () => {
        async function op(){
            
            if (id){
                const json = await api.post(`localOperacaoId`, {"id":id});
                for (let i of json.data){
            
                    setTitulo(i.titulo);
                    setCota(i.cotaAprovada);
                    setInformacao(i.informacao);
                    setCoint(i.coint);
                    setOpm(i.opm);
                    setLinhaTitulo(i.linhaTitulo);
                    setLinhaMes(i.linhaMes);
                    setLinhaRef1(i.linhaRef1);
                    setLinhaRef2(i.linhaRef2);
                    setDataIni(i.dataIni);
                    setDataTer(i.dataTer);
                    setLinhaAssinData(i.linhaAssinData);
                    setLinhaAssinNome(i.linhaAssinNome);                                 
                }
            }
        }
        op();
    },[id]);

    async function handleRegister(e){
        e.preventDefault();
        setStatus(true);

        const data = {
            id,
            titulo,
            cotaAprovada,
            informacao,

            coint,
            opm,
            linhaTitulo,
            linhaMes,
            linhaRef1,
            linhaRef2,
            dataIni,
            dataTer,
            linhaAssinData,
            linhaAssinNome,
            status
        };
        if (!data.id){data.id = -1;}
        
        try{
            data.ini = (new Date(dataIni + " 00:00:00").getTime()/1000);
            data.fim = (new Date(dataTer + " 00:00:00").getTime()/1000);
            data.cotaUtilizada = 0;
            data.status = true;

            let res = await api.post('localOperacaoN', data ); // posta no banco de dados local
            
            if (res.data.id[0]){
                alert ("Operação cadastrada com sucesso! ");
                history.push(`/missoes/view/${res.data.id[0]}`); 
            }      
        } catch (err){
            console.log(err);
        }
    }
    
    return (
        <div className="new-operacao-container">
            <Link className="voltar" to="/profile">
                <FiArrowLeft size="16" color="#e02041"></FiArrowLeft>
            </Link>
            <section className="form">
                <form onSubmit={ handleRegister }>
                    <h1>Cadastrar nova operação</h1>
                    
                    <h3>Informações para o usuário</h3>
                    <input
                        placeholder="Título (Exemplo: OPERAÇÃO BOAS FESTAS)" 
                        value = {titulo}
                        onChange = {e => setTitulo(e.target.value)}
                        required
                    />
                    <input 
                        type="number" 
                        placeholder="Cota aprovada para unidade"
                        value = {cotaAprovada}
                        onChange = {e => setCota(e.target.value)}
                        required
                    />
                    <textarea 
                        placeholder="Lembretes..." 
                        value = {informacao}
                        onChange = {e => setInformacao(e.target.value)}
                    />
                    <h3>Informações para impressão na planilha</h3>
                    <input
                        placeholder="Coint" 
                        value = {coint}
                        onChange = {e => setCoint(e.target.value)}
                    />
                    <input
                        placeholder="OPM" 
                        value = {opm}
                        onChange = {e => setOpm(e.target.value)}
                    />
                    <input
                        placeholder="Linha Título da planilha" 
                        value = {linhaTitulo}
                        onChange = {e => setLinhaTitulo(e.target.value)}
                    />
                    <input
                        placeholder="Linha Mês (Exemplo: 12/21)" 
                        value = {linhaMes}
                        onChange = {e => setLinhaMes(e.target.value)}
                    />
                    <input
                        placeholder="Linha Ref 01: (Exemplo: O. S. Nº 060/2020-PREV/DGO)" 
                        value = {linhaRef1}
                        onChange = {e => setLinhaRef1(e.target.value)}
                    />
                    <input
                        placeholder="Linha Ref 02: (Exemplo:  O. S. Nº 058/2020/-CPR VIII - OPERAÇÃO)" 
                        value = {linhaRef2}
                        onChange = {e => setLinhaRef2(e.target.value)}
                    />
                    <div>
                        <div className='col1'><label>Data início do período</label></div>
                        <div className='col2'><input 
                            type="date" 
                            placeholder="Início" 
                            value = {dataIni}
                            onChange = {e => setDataIni(e.target.value)}
                            required
                        /></div>
                    </div>
                    <div>
                        <div className='col1'><label>Data fim do período</label></div>
                        <div className='col2'><input 
                            type="date" 
                            placeholder="Término" 
                            value = {dataTer}
                            onChange = {e => setDataTer(e.target.value)}
                            required
                        /></div>
                    </div>
                    <input
                        placeholder="Linha data da assinatura (Exemplo:  21/12/20)" 
                        value = {linhaAssinData}
                        onChange = {e => setLinhaAssinData(e.target.value)}
                    />
                    <input
                        placeholder="Linha assinatura (Exemplo:  TEN CEL HENRIQUE)" 
                        value = {linhaAssinNome}
                        onChange = {e => setLinhaAssinNome(e.target.value)}
                    />
                    { id
                        ? <button className="button" type="submit">Atualizar</button>
                        : <button className="button" type="submit">Cadastrar</button>
                    }
                </form>
            </section>
        </div>
    );
}