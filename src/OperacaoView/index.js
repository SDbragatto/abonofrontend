import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import {FiArrowLeft, FiTrash2, FiEye} from 'react-icons/fi';
//https://react-icons.github.io/react-icons/icons?name=fi

import api from "../service/api";

import './style.css';

export default function Profile(){
    let data;
    const [operacao, setOperacao] = useState([]);
    //const [numero, setNumero] = useState("");
    const [status , setStatus] = useState([]);
    //const [loading, setLoading] = useState(false);


    useEffect( () => {
        async function op(){
            let dadosOp = [];
            const json = await api.get('localOperacoesArquivada');
            for (let i of json.data){
                i.idOperacao = i.id;
                dadosOp[i.id] = i;
                dadosOp[i.id].cotaUtilizada = 0;
                dadosOp[i.id].missoes = [];
                const jsonm = await api.post('localMissoesPelaOperacao', i ); 
                
                for (let ii of jsonm.data){
                    dadosOp[i.id].missoes[ 'M' + ii.idM ] = ii;
                    let guarnicao = JSON.parse(ii.guarnicao);
                    dadosOp[i.id].cotaUtilizada += guarnicao.length;
                }                
            }
            setOperacao(dadosOp);
        }
        op();
    }, [status]);

    async function excluirOperacao(id){
        
        if (window.confirm("Deseja excluir: "+operacao[id].titulo +"?")) {
            //setNumero(id); // ativa o icon de loading
            //setLoading(true);
            data = {
                id
            };
            try{
                await api.post('excluirOperacao', data ); // posta no banco de dados local
                setStatus(id);
                //setLoading(false);
            } catch (err){
                console.log(err);
            }
        }
    }

    async function arquivarOperacao(id){
        if (window.confirm("Deseja desarquivar: "+operacao[id].titulo +"?")) {
            //setNumero(id); // ativa o icon de loading
            // setLoading(true);
            data = {
                id
            };
            data.status = true;
            try{
                await api.post('arquivarOperacao', data ); // posta no banco de dados local
                setStatus(id);
                //setNumero("");
                //setLoading(false);
            } catch (err){
                console.log(err);
            }
        }
    }

    return(
        <div className="profile-container">
            <Link className="voltar" to="/profile">
                <FiArrowLeft size="16" color="#e02041"></FiArrowLeft>
            </Link>
            <h1>Operações Arquivadas</h1>
            <ul>
                { operacao.length > 0 && operacao.map( op => (
                    <li key = {op.id}>
                        <strong>Operação: </strong>
                        <p>{op.titulo}</p>

                        <strong>Período: </strong>
                        <p>DE {op.dataIni} ATÉ {op.dataTer}</p>

                        <strong>Cota de Abono </strong>
                        <p>Aprovada: {op.cotaAprovada}</p>
                        <p>Utilizada: {op.cotaUtilizada} </p>
                        <p>Saldo: {op.cotaAprovada - op.cotaUtilizada}</p>

                        <strong>Total de Missões: </strong>
                        <p>{Object.keys(op.missoes).length}</p>

                        <button  onClick={() => excluirOperacao(op.id) }  type="button" className="minButton" title="excluir operação">
                            <FiTrash2 size={20} color="#a8a8b3" />
                        </button>
                        <button  onClick={() => arquivarOperacao(op.id) }  type="button" className="minButtonOcult" title="desarquivar operação">
                            <FiEye size={20} color="#a8a8b3" />
                        </button>
                        <Link className="button maxButton" to={`/missoes/view/${op.id}`} >Gerenciar Missões</Link>
                    </li>
                ))}
            </ul>
            { operacao.length === 0 && 
                <div className="profile-container-div">
                    <h3>Nenhuma operação foi arquivada até o momento!</h3>
                </div>
            }
        </div>
    )
}