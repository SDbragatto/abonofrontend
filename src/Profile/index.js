import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import {FiPower, FiTrash2, FiEdit, FiEyeOff, FiCopy} from 'react-icons/fi';
//https://react-icons.github.io/react-icons/icons?name=fi

import api from "../service/api";

import './style.css';

export default function Profile(){
    let data;
    const [operacao, setOperacao] = useState([]);
    //const [numero, setNumero] = useState("");
    const [status , setStatus] = useState([]);
    //const [loading, setLoading] = useState(false);


    useEffect( () => {
        async function op(){
            let dadosOp = [];
            const json = await api.get('localOperacoes');
            for (let i of json.data){
                i.idOperacao = i.id;
                dadosOp[i.id] = i;
                dadosOp[i.id].cotaUtilizada = 0;
                dadosOp[i.id].missoes = [];
                const jsonm = await api.post('localMissoesPelaOperacao', i ); 
                
                for (let ii of jsonm.data){
                    dadosOp[i.id].missoes[ 'M' + ii.idM ] = ii;
                    let guarnicao = JSON.parse(ii.guarnicao);
                    dadosOp[i.id].cotaUtilizada += guarnicao.length;
                }
            }
            setOperacao(dadosOp);
        }
        op();
    }, [status]);

    async function excluirOperacao(id){
        
        if (window.confirm("Deseja excluir: "+operacao[id].titulo +"?")) {
            //setNumero(id); // ativa o icon de loading
            //setLoading(true);
            data = {
                id
            };
            try{
                await api.post('excluirOperacao', data ); // posta no banco de dados local
                setStatus(id);
                //setLoading(false);
            } catch (err){
                console.log(err);
            }
        }
    }

    async function handleRegister(id){
        setStatus(id);
        console.log()
        const data = {
            id : -1,
            titulo : operacao[id].titulo,
            cotaAprovada : operacao[id].cotaAprovada ,
            informacao : operacao[id].informacao,

            coint : operacao[id].coint,
            opm : operacao[id].opm,
            linhaTitulo : operacao[id].linhaTitulo,
            linhaMes : operacao[id].linhaMes,
            linhaRef1 : operacao[id].linhaRef1,
            linhaRef2 : operacao[id].linhaRef2,
            dataIni : operacao[id].dataIni,
            dataTer : operacao[id].dataTer,
            linhaAssinData : operacao[id].linhaAssinData,
            linhaAssinNome : operacao[id].linhaAssinNome,
            status : true
        };
        
        try{
            data.ini = (new Date(operacao[id].dataIni + " 00:00:00").getTime()/1000);
            data.fim = (new Date(operacao[id].dataTer + " 00:00:00").getTime()/1000);
            data.cotaUtilizada = 0;
            data.status = true;

            //let res = await api.post('localOperacaoN', data ); // posta no banco de dados local
            /*
            if (res.data.id[0]){
                alert ("Operação cadastrada com sucesso! ");
                history.push(`/missoes/view/${res.data.id[0]}`); 
            } 
            */     
        } catch (err){
            console.log(err);
        }
    }

    async function arquivarOperacao(id){
        if (window.confirm("Deseja arquivar: "+operacao[id].titulo +"?")) {
            //setNumero(id); // ativa o icon de loading
            //setLoading(true);
            data = {
                id
            };
            data.status = false;
            try{
                await api.post('arquivarOperacao', data ); // posta no banco de dados local
                setStatus(id);
                //setLoading(false);
            } catch (err){
                console.log(err);
            }
        }
    }

    return(
        <div className="profile-container">
            <header>
                <span>Bem vindo ao Abono</span>
                <Link to="#"/>
                <button type="button">
                    <FiPower size={18} color="#e02041" />
                </button>
            </header>
            <div>
                <h1>Operações em andamento</h1>
                <Link className="button secundario" to="/operacao/view" > Desarquivar Operações </Link>
                <Link className="button" to="/novaoperacao" >Cadastrar Operação </Link> 
                
            </div>
            <ul>
                { operacao.length > 0 && operacao.map( op => (
                    <li key = {op.id}>
                        <strong>Operação: </strong>
                        <p>{op.titulo}</p>

                        <strong>Período: </strong>
                        <p>DE {op.dataIni} ATÉ {op.dataTer}</p>

                        <strong>Cota de Abono </strong>
                        <p>Aprovada: {op.cotaAprovada}</p>
                        <p>Utilizada: {op.cotaUtilizada} </p>
                        <p>Saldo: {op.cotaAprovada - op.cotaUtilizada}</p>

                        <strong>Total de Missões: </strong>
                        <p>{Object.keys(op.missoes).length}</p>

                        <button  onClick={() => excluirOperacao(op.id) }  type="button" className="minButton" title="excluir operação">
                            <FiTrash2 size={20} color="#a8a8b3" />
                        </button>
                        <button  onClick={() => arquivarOperacao(op.id) }  type="button" className="minButtonOcult" title="arquivar operação">
                            <FiEyeOff size={20} color="#a8a8b3" />
                        </button>
                        <button  onClick={() => handleRegister(op.id) }  type="button" className="minButtonCopie" title="copiar operação">
                            <FiCopy size={20} color="#a8a8b3" />
                        </button>
                        <Link className="minButtonEditar" to={`/editOperacao/${op.id}`} title="editar operação" >
                            <FiEdit size={20} color="#a8a8b3" />
                        </Link>
                        
                        <Link className="button maxButton" to={`/missoes/view/${op.id}`} >Gerenciar Missões</Link>
                    </li>
                ))}
            </ul>
            { operacao.length === 0 && 
                <div className="profile-container-div">
                    <h3>Para iniciar, cadrastre uma Operação!</h3>
                </div>
            }
        </div>
    )
}