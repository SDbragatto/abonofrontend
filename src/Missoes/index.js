import React, { useEffect, useState } from 'react';
import {Link, useParams} from 'react-router-dom';
import {FiArrowLeft, FiTrash2, FiDownload, FiRefreshCw} from 'react-icons/fi';

import api from "../service/api";
import './style.css';

function NewDate(str){
    // formato recebido ( 23/12/21 20:00 )
    var year = str.substr(0, 8).split('/');
    var hour = str.substr(9, 12).split(':');
    var date=new Date("20"+year[2], year[1]-1, year[0], hour[0], hour[1] );
    return date;
    }

function prepraraMissão(resp, numero){
    let missao = {}, guarnicao;

    guarnicao = resp.data.missao.guarnicao.reduce(function(allMissões, missao){ // cria a array da guarnição
        if (!allMissões['cpfs']) {
            allMissões['cpfs'] = [];
        }
        if (missao.cpf !== null){
            allMissões['cpfs'].push(missao.cpf);
        }
        return allMissões;
    }, {});

    missao[numero] = resp.data.missao; // armazena a missão no data
    missao[numero].idM = numero; // armazena o numero da missão no data
    missao[numero].guarnicao = JSON.stringify(guarnicao.cpfs); // armazena a guarnição no data
    missao[numero].ini = (NewDate(resp.data.missao.dt_inicio_missao).getTime()); // armazena a data inicial no data
    missao[numero].fim = (NewDate(resp.data.missao.dt_fim_missao).getTime()); // armazena a data final no data
    return missao[numero];
}

function prepraraDadosPM(resp, cpf){
    let Objeto = {};
    Objeto.idCPF = cpf; // armazena o numero da missão no data
    Objeto.nome_guerra = resp.data.nome_guerra; // armazena a guarnição no data
    Objeto.nome = resp.data.nome; // armazena a data inicial no data
    Objeto.rg = resp.data.rg; // armazena a data final no data
    Objeto.matricula = resp.data.matricula ;
    Objeto.graduacao = resp.data.Graduacao.graduacao;
    return Objeto;
}

export default function Missoes(){
    let data, resp, dadosM;
    const { id } = useParams(); // pega a parms na url
    const [numero, setNumero] = useState(""); // recebe o numero digitdo no formulario
    const [missoes, setMissoes] = useState([]);
    const [operacao, setOperacao] = useState([]);
    const [guarnicao, setGuarnicao] = useState([]);
    const [status , setStatus] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect( () => {
        async function op(){
            let dadosOp = [], dadosMi = {};
            const json = await api.post(`localOperacaoId`, {"id":id});

            for (let i of json.data){
                
                dadosOp[i.id] = i;
                dadosOp[i.id].contMissoes = 0;
                dadosOp[i.id].cotaUtilizada = 0;
                
                i.idOperacao = i.id;
                const jsonM = await api.post('localMissoesPelaOperacao', i); 
                
                for (let ii of jsonM.data){
                    let guarnicao = JSON.parse(ii.guarnicao);
                    ii.guarnicao = guarnicao;
                    dadosMi[ii.idM] = ii;

                    dadosOp[i.id].cotaUtilizada += guarnicao.length;
                    dadosOp[i.id].contMissoes ++;
                }                
            }
            setOperacao(dadosOp);
            setMissoes(dadosMi);
        }
        op();
    },[status, id]);
    
    useEffect( () => {
        async function cpfs(){
            let dadosCPFs = {};
            const json = await api.get(`localCPFs`);
            for (let i of json.data){
                dadosCPFs[i.idCPF] = i;   
            }       
            setGuarnicao(dadosCPFs);
        }
        cpfs();
    },[status]);

    async function handleRegisterMissao(e){
        setLoading(true);
        e.preventDefault();
        data = {
            numero
        };
        console.log(data);
        try{
            await api.post('login'); // confirma o login no sigpol para gerar o token
            resp = await api.post('missao', data);  // consulta o numero da missao no sigpol
           
            if (resp.data.missao.unidade){ // caso retorne uma missão // 2020105552
                data = prepraraMissão(resp, numero); // organiza os dados para armazer no banco de dados local
                data.idOperacao = id; 
                console.log(data);            
                await api.post('localMissao', data ); // posta no banco de dados local
                setStatus(new Date().getTime());
                setNumero("");
                setLoading(false);
            }
        } catch (err){
            console.log(err);
        }
    }

    async function handleRegisterMissaoUP(missao){
        setNumero(missao); // ativa o icon de loading
        setLoading(true);
        data = {};
        data.numero = missao;
        try{
            await api.post('login'); // confirma o login no sigpol para gerar o token
            resp = await api.post('missao', data);  // consulta o numero da missao no sigpol
            if (resp.data.missao.unidade){ // caso retorne uma missão // 2020105552
                dadosM = prepraraMissão(resp, missao); // organiza os dados para armazer no banco de dados local
                dadosM.idOperacao = id;
                console.log(dadosM);
                await api.post('localMissao', dadosM ); // posta no banco de dados local
                setStatus(new Date().getTime());
                setNumero("");
                setLoading(false);
            }
        } catch (err){
            console.log(err);
        }
    }

    async function handleRegisterCPF(cpf){
        setNumero(cpf); // ativa o icon de loading
        setLoading(true);
        try{
            await api.post('login'); // confirma o login no sigpol para gerar o token
            resp = await api.post('cpf', { "numero": cpf });  // consulta o numero da missao no sigpol
            
            if (resp.data.cpf){ // caso retorne um cpf
                data = prepraraDadosPM(resp, cpf); // organiza os dados para armazer no banco de dados local
                await api.post('localCPFs', data ); // posta no banco de dados local
                setStatus(new Date().getTime());
                setNumero("");
                setLoading(false);
            }
        } catch (err){
            console.log(err);
        }
    }

    async function excluirMissao(n){
        
        if (window.confirm("Deseja excluir a missão: "+ n +"?")) {
            setNumero(n); // ativa o icon de loading
            setLoading(true);
            data = { };
            data.idM = n;
            try{
                await api.post('excluirMissao', data ); // posta no banco de dados local
                setStatus(new Date().getTime());
                setNumero("");
                setLoading(false);
            } catch (err){
                console.log(err);
            }
        }
    }

    function handleConsultCPF(mi, cpf) {
        let container;
        if (guarnicao[cpf]){
            container = <p className='efetivo'>
                {guarnicao[cpf].graduacao} PM RG {guarnicao[cpf].rg } {guarnicao[cpf].nome_guerra}
                <button id={'b'+ mi + cpf} onClick={ () => handleRegisterCPF(cpf)} type="button" className="buttonUp" title="Baixar dados do SIGPOL" disabled={loading}>
                    { cpf===numero && <FiRefreshCw className="spinner" size={15} color="#fff" /> }
                    { cpf!==numero && <FiDownload size={15} color="#fff" /> }
                </button>
            </p>;
        }else{
            container = 
            <button id={cpf} onClick={ () => handleRegisterCPF(cpf)} type="button" className="button" title="Baixar dados do SIGPOL" disabled={loading}>
            Baixar dados do CPF {cpf} no SIGPOL 
            { cpf===numero && <FiRefreshCw className="spinner" size={20} color="#fff" /> }
            { cpf!==numero && <FiDownload size={20} color="#fff" /> }
        </button>;
        }

        return container;
    }
    

    return (
        <div className="missoes-container">
            <Link className="voltar" to="/profile">
                <FiArrowLeft size="16" color="#e02041"></FiArrowLeft>
            </Link>
            
            <section className="form">
                <h1>Gerenciar Missões</h1>
                <div className="grid">
                    
                    {operacao.length > 0 && operacao.map( op => (
                    <div key={op.id}>
                        <strong>Operação: </strong>
                        <p>{op.titulo}</p>

                        <strong>Período: </strong>
                        <p>DE {op.dataIni} ATÉ {op.dataTer}</p>

                        <strong>Cota de Abono </strong>
                        <p>Aprovada: {op.cotaAprovada}</p>
                        <p>Utilizada: {op.cotaUtilizada} </p>
                        <p>Saldo: {op.cotaAprovada - op.cotaUtilizada}</p>

                        <strong>Total de Missões: </strong>
                        <p>{op.contMissoes}</p>

                    </div>
                    ))}
                    <div>
                        <form onSubmit={handleRegisterMissao}>
                            <strong>Adicionar nova missão</strong>
                            <input
                                id="numero"
                                type="text"
                                pattern="[0-9]{10}"
                                title = "O número da missão possui 10 caracteres"
                                placeholder="digite o número da missão"
                                value = {numero}
                                onChange = {e => setNumero(e.target.value)}
                                required
                                />
                            <button className="button" type="submit" disabled={loading} >
                                { !loading && "Consultar missão / Adicionar"  }
                                { loading && <FiRefreshCw className="spinner" size={20} color="#fff" /> }
                            </button>
                        </form>
                        <p>Será realizado uma consulta no banco de dados do SIGPOL.</p>
                        <p>Caso o número da missão retorne dados positivos, os mesmos serão armazenados no banco de dados local.</p>
                    </div>
                </div>    
            </section>

            <section className="form">
                <div>
                    <h1>Visualização</h1>    
                </div>
                <div className="grid3">
                    <div>
                        <Link className="button secundario" to={`/planilha/operacao/${id}`} >  Planilha Final </Link>
                    </div>
                    <div>
                        <Link className="button secundario" to="/planilhaGeral" > Todas as planilhas ativas </Link>
                    </div>
                    <div>
                        <Link className="button secundario" to="/efetivoGeral" > Todo o efetivo escalado </Link>
                    </div>
                </div>
            </section>

            <section className="form">
                <div>
                    <h1>Missões Adicionadas</h1>    
                </div>
                <ul>
                    { Object.keys(missoes).map( mi => ( 
                        <li key = {'l'+mi}>
                            <strong>Missão: {mi}</strong>                          
                            <p>....</p>
                            <button onClick={() => handleRegisterMissaoUP(mi) } type="button" className="buttonUp" title="Baixar dados do SIGPOL" disabled={loading}>
                                { mi===numero && <FiRefreshCw className="spinner" size={20} color="#a8a8b3" /> }
                                { mi!==numero && <FiDownload size={20} color="#a8a8b3" /> }
                            </button>
                            <button onClick={() => excluirMissao(mi)} type="button" className="buttonLixo" >
                                <FiTrash2 size={20} color="#a8a8b3" />
                            </button>
                            
                            <strong>Data/Hora: </strong>
                            <p>{missoes[mi].dt_inicio_missao} até {missoes[mi].dt_fim_missao}</p>
                        
                            <strong>Efetivo escalado: {Object.keys(missoes[mi].guarnicao).length}</strong>
                            {
                                missoes[mi].guarnicao.map( cpf =>(
                                    <div key = {mi + cpf} >
                                        { handleConsultCPF(mi, cpf) } 
                                    </div>
                                ))
                                
                            }
                        </li>
                    ))
                    }
                </ul>
            </section>

        </div>
    );
}