import React, { useEffect, useState } from 'react';
import {Link, useParams} from 'react-router-dom';
import {FiArrowLeft} from 'react-icons/fi';

import { jsPDF } from "jspdf";
import api from "../service/api";
import './style.css';

export default function PlanilhaOperacao(){

    //let data, resp;
    const { id } = useParams(); // pega a parms na url
    //const [numero, setNumero] = useState(""); // recebe o numero digitdo no formulario
    const [missoes, setMissoes] = useState([]);
    const [operacao, setOperacao] = useState([]);
    const [guarnicao, setGuarnicao] = useState([]);
    //const [planilha, setPlanilha] = useState([]);
    //const [status , setStatus] = useState([]);
    //const [loading, setLoading] = useState(false);
    const [dataIni, setDataIni] = useState('');
    const [dataTer, setDataTer] = useState('');
    
    useEffect( () => {
        async function op(){
            let dadosOp = {}, dadosMi = {};
            let ord = 0;
            const json = await api.post(`localOperacaoId`, {"id":id});

            for (let i of json.data){

                dadosOp = i;

                const jsonM = await api.post('localMissoesPelaOperacao', {idOperacao : id}); 

                for (let ii of jsonM.data){
                    ord++;
                    let guarnicao = JSON.parse(ii.guarnicao);
                    ii.guarnicao = guarnicao;
                    dadosMi[ord] = ii;
                }                
            }
            setOperacao(dadosOp);
            setMissoes(dadosMi);            
        }
        op();

    },[id]);
    
    useEffect( () => {
        async function cpfs(){
            let dadosCPFs = {};
            
            const json = await api.get(`localCPFs`);
            
            for (let i of json.data){ 
                dadosCPFs[i.idCPF] = i;                   
            }
            setGuarnicao(dadosCPFs);
        }
        cpfs();
    },[]);

    function handlePDF(e){
        e.preventDefault();
               
        function data(dataIni, dataFim){
            let ini = dataIni.split('-');
            let fim = dataFim.split('-');
            return (ini[2]+'/'+ini[1]+'/'+ini[0]+' até '+fim[2]+'/'+fim[1]+'/'+fim[0]);
        }

        function proxima(){
            config.atual = config.atual + ( config.sizefont / 2 );
            return config.atual;
        }

        function celulas(texto, coluna, formatacao, tamanho, tdAtual, tdH){
            switch(formatacao){
                case "center": pdf.text(texto, coluna + (tamanho / 2), tdAtual, formatacao);
                 break;
                case "left": pdf.text(texto, coluna + 2, tdAtual, formatacao);
                 break;
                case "right": pdf.text(texto, coluna + tamanho - 2, tdAtual, formatacao);
                 break;
                default: proxima();
                 break;
            }
            pdf.rect(coluna, ( config.atual - config.td ), tamanho, tdH );
        }

        function cabecalho(){
            pdf.setFontSize(10);
            pdf.text('COint: ' , 45, 10, 'right' );
            pdf.text(operacao.coint, 106, 10, 'center');
            pdf.rect(46, 6, 120, config.tdH );

            pdf.text('OPM: ' , 45, 15, 'right' );
            pdf.text(operacao.opm, 106, 15, 'center');
            pdf.rect(46, 11, 120, config.tdH );
        }

        function cabecalhoTabela(){
            
            celulas('Nº', config.margin, 'center', 8, config.atual + (config.tdH / 2), config.tdH * 2);
            celulas('Lotação', config.margin + 8, 'center', 18, config.atual + (config.tdH / 2), config.tdH * 2);
            celulas('Posto /\rGrad.', config.margin + 26, 'center', 18, config.atual, config.tdH * 2);
            celulas('Nome', config.margin + 44, 'center', 84, config.atual + (config.tdH / 2), config.tdH * 2);
            celulas('Nome de\rGuerra', config.margin + 128, 'center', 25, config.atual, config.tdH * 2);
            celulas('Matrícula\rFuncional', config.margin + 153, 'center', 20, config.atual, config.tdH * 2);
            celulas('DATAS DAS JORNADAS', config.margin + 173, 'center', 81, config.atual, config.tdH * 2);
            celulas('Qtd JO', config.margin + 254, 'center', 13, config.atual + (config.tdH / 2), config.tdH * 2);
            celulas('Valor (R$)', config.margin + 267, 'center', 20, config.atual + (config.tdH / 2), config.tdH * 2);
            proxima();
            celulas('1ª', config.margin + 173, 'center', 10, config.atual, config.tdH);
            celulas('2ª', config.margin + 183, 'center', 10, config.atual, config.tdH);
            celulas('3ª', config.margin + 193, 'center', 10, config.atual, config.tdH);
            celulas('4ª', config.margin + 203, 'center', 10, config.atual, config.tdH);
            celulas('5ª', config.margin + 213, 'center', 10, config.atual, config.tdH);
            celulas('6ª', config.margin + 223, 'center', 10, config.atual, config.tdH);
            celulas('7ª', config.margin + 233, 'center', 10, config.atual, config.tdH);
            celulas('8ª', config.margin + 243, 'center', 11, config.atual, config.tdH);
           
        }

        function total(){
            proxima();
            celulas('TOTAL '+ pdf.getNumberOfPages(), config.margin + 223, 'center', 31, config.atual, config.tdH);
            celulas(String(soma), config.margin + 254, 'center', 13, config.atual, config.tdH);
            celulas((soma * 185.40).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}), config.margin + 267, 'right', 20, config.atual, config.tdH);
            
            if (pdf.getNumberOfPages() > 1){
                totalGeral();
            }          
        }

        function totalGeral(){
            proxima();
            celulas('TOTAL 1 a '+ pdf.getNumberOfPages(), config.margin + 223, 'center', 31, config.atual, config.tdH);
            celulas(String(somaGeral), config.margin + 254, 'center', 13, config.atual, config.tdH);
            celulas((somaGeral * 185.40).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}), config.margin + 267, 'right', 20, config.atual, config.tdH);
        }

        function assinatura(){
            config.atual = 190;
            celulas(operacao.linhaAssinData, config.margin + 8, 'center', 36, config.atual, config.tdH);
            proxima();
            pdf.text(operacao.linhaAssinNome, 148, config.atual, 'center');
        }

        function pagina(){
            config.atual = 200;
            celulas('Pág:', config.margin + 254, 'center', 13, config.atual, config.tdH);
            celulas(String(pdf.getNumberOfPages()), config.margin + 267, 'center', 20, config.atual, config.tdH);
        }

        // constroi os dados da planilha
        let dadosPlanilha = [];
        let dia;
        let getTime;
        let dataIniGetTime = new Date(dataIni + " 00:00").getTime();
        let dataTerGetTime = new Date(dataTer + " 23:59").getTime();
                
        for (let m in missoes){
            dia = missoes[m].dt_inicio_missao.substr(0, 5);
            getTime = missoes[m].ini;
            if (getTime > dataIniGetTime && getTime < dataTerGetTime ) {
                for (let cpf of missoes[m].guarnicao){
                    if (!dadosPlanilha[cpf]){
                        dadosPlanilha[cpf] = [];
                        dadosPlanilha[cpf].getTimeIni = getTime;
                        dadosPlanilha[cpf].quant = 0;
                        dadosPlanilha[cpf].dia = {};
                        for (let i=1; i <= 8; i++ ){
                            dadosPlanilha[cpf].dia[i] = "";
                        }
                    }
                    dadosPlanilha[cpf].quant ++;
                    dadosPlanilha[cpf].dia[ dadosPlanilha[cpf].quant ] = dia;
                }
            }
        }
        dadosPlanilha.sort(function (a, b){
            return (a.getTimeIni > b.getTimeIni) ? 1 : ((b.getTimeIni > a.getTimeIni) ? -1 : 0);
        });

        // Default export is a4 paper, portrait, using millimeters for units
        let pdf, config, pm, i;
        let soma = 0;
        let somaGeral = 0;
        
        config = {			
            "height" : 297, // 
            "width" : 210,
            "sizefont" : 10,
            "td" : 3.7, // centraliza na celula
            "tdH" : 5,
            "atual" : 5, //46
            "margin" : 5
        }

        pdf = new jsPDF('l', 'mm', [config.width, config.height]);
        pdf.setFontSize(config.sizefont);
        pdf.setProperties({
            title: 'ABONO'
        });
        
        cabecalho();
        proxima();
        proxima();
        proxima();
        proxima();
        celulas(operacao.linhaTitulo, config.margin, 'center', 247, config.atual + 0.2, config.tdH);
        celulas(operacao.linhaMes, 252, 'center', 40, config.atual + 0.2, config.tdH);
        proxima();
        pdf.text('Ref: ' , config.margin + 2, config.atual + (config.tdH / 2), 'left' );
        celulas(operacao.linhaRef1 + '\r' + operacao.linhaRef2, config.margin, 'center', 237, config.atual + 0.5, config.tdH * 2);
        celulas(data(dataIni, dataTer), 242, 'center', 50, config.atual + (config.tdH / 2), config.tdH * 2);
        proxima();
        proxima();
        proxima();
        cabecalhoTabela();
        // listar policiais militares
        i = 1;
        
        for (pm in dadosPlanilha){
            
            pdf.setFontSize(8);
            soma += dadosPlanilha[pm].quant;
            proxima();
            celulas(String(i), config.margin, 'center', 8, config.atual, config.tdH);
            celulas('16º BPM', config.margin + 8, 'center', 18, config.atual, config.tdH);
            celulas(guarnicao[pm].graduacao, config.margin + 26, 'center', 18, config.atual, config.tdH);
            
            (String(guarnicao[pm].nome).length < 45)? pdf.setFontSize(8): pdf.setFontSize(7);
            celulas(guarnicao[pm].nome, config.margin + 44, 'left', 84, config.atual, config.tdH);
            
            (String(guarnicao[pm].nome_guerra).length < 14)? pdf.setFontSize(8): pdf.setFontSize(7);
            celulas(guarnicao[pm].nome_guerra, config.margin + 128, 'center', 25, config.atual, config.tdH);
            
            pdf.setFontSize(8);
            celulas(guarnicao[pm].matricula, config.margin + 153, 'center', 20, config.atual, config.tdH);
            celulas(String(dadosPlanilha[pm].quant) , config.margin + 254, 'center', 13, config.atual, config.tdH);
            celulas((dadosPlanilha[pm].quant * 185.40).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}), config.margin + 267, 'right', 20, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[1], config.margin + 173, 'center', 10, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[2], config.margin + 183, 'center', 10, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[3], config.margin + 193, 'center', 10, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[4], config.margin + 203, 'center', 10, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[5], config.margin + 213, 'center', 10, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[6], config.margin + 223, 'center', 10, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[7], config.margin + 233, 'center', 10, config.atual, config.tdH);
            celulas(dadosPlanilha[pm].dia[8], config.margin + 243, 'center', 11, config.atual, config.tdH);
            i++;
            if ( !(dadosPlanilha.length == i) && (config.atual + 55) > config.width){
                somaGeral += soma;
                total();
                assinatura()
                pagina();
                
                pdf.addPage();
                cabecalho();
                config.atual = 30;
                cabecalhoTabela();
                soma = 0; 
            }
        }
        somaGeral += soma;
        total();
        assinatura()
        pagina();

        pdf.output('dataurlnewwindow', "Abono");
    }

    return(
        <div className="missoes-container">
            <Link className="voltar" to={`/missoes/view/${id}`}>
                <FiArrowLeft size="16" color="#e02041"></FiArrowLeft>
            </Link>
            <section>
                <form onSubmit={ handlePDF }>
                    <h1>Gerar PDF</h1>
                        
                    <h3>Definir o intervalo de tempo para gerar planilha</h3>
               
                    <div>
                        <div className='col1'><label>Data início do período</label></div>
                        <div className='col2'><input 
                            type="date" 
                            placeholder="Início" 
                            value = {dataIni}
                            onChange = {e => setDataIni(e.target.value)}
                            required
                        /></div>
                    </div>
                    <div>
                        <div className='col1'><label>Data fim do período</label></div>
                        <div className='col2'><input 
                            type="date" 
                            placeholder="Término" 
                            value = {dataTer}
                            onChange = {e => setDataTer(e.target.value)}
                            required
                        /></div>
                    </div>
                    <div className='missoes-button'>
                        <button className="button" type="submit" > Gerar PDF </button>
                    </div>
                </form>
            </section>
        </div>
    )
}
