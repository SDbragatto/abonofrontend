import React from 'react';

import './style.css';

export default function Logon(){
    return (
        <div className="logon-container">
            <section className="form">
                <form>
                    <h1>Faça o logon</h1>
                    <input placeholder="CPF" />
                    <input placeholder="senha" />
                    <button className="button" type="submit">Entrar</button>
                </form>
            </section>
        </div>
    );
}